package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Separator;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class View {
    @FXML
    private Separator borderUp;
    @FXML
    private Separator borderDown;
    @FXML
    private GridPane gridBorderUp;
    @FXML
    private GridPane gridBorderDown;
    @FXML
    private ToggleButton btnProperty;
    @FXML
    private SplitPane splitPane;
    @FXML
    private ToggleButton btnTable;
    @FXML
    private SplitPane splitTable;

    public void setViewSeparators() {
        borderUp.setMaxWidth(Double.MAX_VALUE);
        gridBorderUp.setHgrow(borderUp, Priority.ALWAYS);
        borderDown.setMaxWidth(Double.MAX_VALUE);
        gridBorderDown.setHgrow(borderUp, Priority.ALWAYS);
    }

    public void setViewPanes() {
        splitPane.setDividerPositions(0.1);
    }

    public void btnPropSetOnAction() {
        btnProperty.setOnAction(actionEvent -> {
            if (btnProperty.isSelected()) {
                splitPane.setDividerPositions(0.2);
            } else {
                splitPane.setDividerPositions(0.0);
            }
        });
    }

    public void btnTableSetOnAction() {
        btnTable.setOnAction(actionEvent -> {
            if (btnTable.isSelected()) {
                splitTable.setDividerPositions(0.8);
            } else {
                splitTable.setDividerPositions(1.0);
            }
        });
    }
}
