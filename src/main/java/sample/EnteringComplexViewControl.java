package sample;

import KvetkaFolder.EnteringComplexControl;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;

public class EnteringComplexViewControl extends AnchorPane {

    EnteringComplexViewController enteringComplexViewController;
    //private final MainController mainController;

    public EnteringComplexViewControl() {
        super();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/EnteringComplexView.fxml"));
            enteringComplexViewController =new EnteringComplexViewController();

//mainController = new MainController();

            loader.setController(enteringComplexViewController);
            Node node = loader.load();
            this.getChildren().add(node);
            enteringComplexViewController.getComplexControl().AddListener(new MainController());
        }
        catch (Exception ioException) {
            throw new RuntimeException(ioException);
        }
    }

    public EnteringComplexControl getEnteringComplexViewController() {
        return enteringComplexViewController.getComplexControl();
    }


    /*@Override
    public void onSubmitted(String s, String s1, String s2) {
        String ss = s;
        String ss1 = s1;
        String ss2 = s2;
        mainController.getComponentLeftPane().setAccessibleText(ss);
        mainController.getComponentLeftPane().setAccessibleText(ss1);
        mainController.getComponentLeftPane().setAccessibleText(ss2);
    }*/
}
