package sample;

import ClientDB.*;
import ClientDB.eventargs.ConnectionEventArgs;
import ClientDB.eventargs.TableEventArgs;
import ClientDB.interfaces.IClientEventsListener;
import ConnectionControl.ConnectionControl;
import ConnectionControl.IConnectionEvent;
import KvetkaFolder.IValueSubmitted;
import KvetkaFolder.EnteringComplexControl;
import KvetkaModels.*;
import javafx.beans.property.DoubleProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class MainController extends View implements IClientEventsListener, IConnectionEvent, IValueSubmitted, Initializable {

    @FXML
    TextField componentLeftPane;
    @FXML
    Separator borderUp;
    @FXML
    Separator borderDown;
    @FXML
    GridPane gridBorderUp;
    @FXML
    GridPane gridBorderDown;
    @FXML
    ToggleButton btnProperty;
    @FXML
    SplitPane splitPane;
    @FXML
    SplitPane splitTable;
    @FXML
    DoubleProperty splitPaneDividerPosition;
    @FXML
    DoubleProperty splitTableDividerPosition;
    @FXML
    Button btOpenEnterComplex;
    @FXML
    ToggleButton btnTable;
    @FXML
    public ConnectionControl ucServerDBConnection;
    @FXML
    EnteringComplexViewControl enteringComplexViewControl;

    private static int id;
    private static String user = "Lina Test";
    private static String serverIp = "localhost";
    private static int serverPort = 30051;
    public ClientDB clientDB;
    List<StationsModel> listStations = new ArrayList<>();

    public void onClickMethod() {
        if (btnProperty.isSelected()) {
            splitPane.setDividerPositions(0.2);
        } else {
            splitPane.setDividerPositions(0.0);
        }
    }

    public Node getComponentLeftPane() {
        return componentLeftPane;
    }

    @Override
    public void initialize(URL Location, ResourceBundle resources) {
        ucServerDBConnection.addListener(this);
        setViewSeparators();
        btOpenEnterComplexEvent();

        HideShowLocalProperty();
        HideShowTable();


    }

    private void btOpenEnterComplexEvent() {
        btOpenEnterComplex.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                enteringComplexViewControl = new EnteringComplexViewControl();

                Stage newStage = new Stage();
                newStage.setTitle("Ввод комплекса");
                newStage.setScene(new Scene(enteringComplexViewControl, 200, 367));
                newStage.show();

            }
        });

        //enteringComplexViewControl.getEnteringComplexViewController().AddListener(this);

    }

    private void HideShowLocalProperty() {
        splitPaneDividerPosition = splitPane.getDividers().get(0).positionProperty();
        splitPaneDividerPosition.addListener((obs, oldPos, newPos) -> {
                    btnProperty.setSelected(newPos.doubleValue() > 0.01);
                }
        );

        splitPaneDividerPosition.set(0.0);
        btnPropSetOnAction();
    }

    private void HideShowTable() {
        splitTableDividerPosition = splitTable.getDividers().get(0).positionProperty();
        splitTableDividerPosition.addListener((obs, oldPos, newPos) -> {
                    btnTable.setSelected(newPos.doubleValue() < 0.99);
                }
        );
        splitTableDividerPosition.set(0.8);
        btnTableSetOnAction();
    }

    @Override
    public void onConnect(ConnectionEventArgs connectionEventArgs) {

        ucServerDBConnection.ShowConnect();
    }

    @Override
    public void onDisconnect(ConnectionEventArgs connectionEventArgs) {

        ucServerDBConnection.ShowDisconnect();
        clientDB = null;
    }

    @Override
    public void OnButtonClick(String nameServer) {

        switch (nameServer) {

            case "ServerDAP":

                break;

            case "ServerDB":

                if (clientDB != null) {

                    clientDB.disconnect();
                } else {

                    clientDB = new ClientDB(user, serverIp, serverPort);
                    InitClientDB();
                    clientDB.connect();
                }
                break;
        }

    }

    private void InitClientDB() {
        clientDB.clientEvents.addListener(this);
        try {
            //clientDB.tables.get(NameTable.TableJammer).onUpdatedTable.addListener((Consumer<TableEventArgs<StationsModel>>) ((tableStation) -> UpdateStations(tableStation)));
            clientDB.tables.get(NameTable.TableJammer).onUpdatedTable.addListener((Consumer<TableEventArgs<StationsModel>>) ((x) -> TablesUpdate(x)));
        } catch (Exception e) {
        }

    }

    private void TablesUpdate(TableEventArgs<StationsModel> x) {
    }

    @Override
    public void onSubmitted(String s, String s1, String s2) {
        String ss = s;
        String ss1 = s1;
        String ss2 = s2;
        componentLeftPane.setText(ss);
        componentLeftPane.setText(ss1);
        componentLeftPane.setText(ss2);
    }

    /*private void UpdateStations(TableEventArgs<StationsModel> tableStations) {
        listStations = tableStations.getTable();
    }*/

    /*private void UpdateStations(TableEventArgs<StationsModel> tableStations) {

        listStations = tableStations.getTable();
    }*/


}
