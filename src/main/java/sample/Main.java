package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.awt.*;

public class Main extends Application {


    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/MainWindow.fxml"));
        Parent root = loader.load();


        Scene scene = new Scene(root);

        primaryStage.setTitle("KvetkaMapWindow");
        primaryStage.setScene(scene);

        //primaryStage.setFullScreen(true);

        primaryStage.setMaxHeight(size.getHeight());
        primaryStage.setMaxWidth(size.getWidth());
        primaryStage.setHeight(size.height);
        primaryStage.setWidth(size.width);
        primaryStage.show();
    }
}




