package sample;

//import KvetkaFolder.EnteringComplexControl;
import KvetkaFolder.EnteringComplexControl;
import KvetkaFolder.IValueSubmitted;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;


public class EnteringComplexViewController implements Initializable {

    @FXML
    private EnteringComplexControl complexControl;

    public EnteringComplexControl getComplexControl() {
        return complexControl;
    }

    public EnteringComplexViewController() {
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

}


